package com.swire.customer_service.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestDataByName {
	private String customerName;

}

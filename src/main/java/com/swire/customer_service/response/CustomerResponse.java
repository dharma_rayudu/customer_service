package com.swire.customer_service.response;

import com.swire.customer_service.request.CustomerRequest;

import lombok.Getter;
import lombok.Setter;

/**
 * For getting the customer response object.
 *
 */

@Getter
@Setter
public class CustomerResponse {

	private String successCode;
	private String successMessage;
	private String status;
	private CustomerRequest requestData;
	private ResponseData responseData;
}

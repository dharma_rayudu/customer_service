package com.swire.customer_service.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestDataByCode {
	
	private String customerCode;

}

package com.swire.customer_service.response;

import com.swire.customer_service.entity.Customer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerResponseByCode {
	
	private String successCode;
	private String successMessage;
	private String status;
	private RequestDataByCode requestData;
	private Customer responseData;
}

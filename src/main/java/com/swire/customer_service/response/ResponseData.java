package com.swire.customer_service.response;

import java.util.List;

import com.swire.customer_service.entity.Customer;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ResponseData {

	private int count;
	List<Customer> rows;

}

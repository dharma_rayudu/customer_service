package com.swire.customer_service.response;

import com.swire.customer_service.entity.Customer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerResponseByName {

	private String successCode;
	private String successMessage;
	private String status;
	private RequestDataByName requestData;
	private Customer responseData;

}

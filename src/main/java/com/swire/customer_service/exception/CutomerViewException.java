package com.swire.customer_service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class CutomerViewException extends Exception {

	public CutomerViewException(String message) {
		super(message);
	}
}

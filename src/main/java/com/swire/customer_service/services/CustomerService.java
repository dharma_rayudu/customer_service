package com.swire.customer_service.services;

import com.swire.customer_service.response.CustomerResponse;
import com.swire.customer_service.response.CustomerResponseByCode;
import com.swire.customer_service.response.CustomerResponseByName;

public interface CustomerService {

	public CustomerResponse getCustomers(String offset, String limit, String sort, String fields, String query, String date_from, String date_to);

	public CustomerResponseByCode getByCode(String cutomerCode);

	public CustomerResponseByName getByName(String customerName);
}

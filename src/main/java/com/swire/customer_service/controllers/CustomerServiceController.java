package com.swire.customer_service.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.swire.customer_service.exception.CutomerViewException;
import com.swire.customer_service.response.CustomerResponse;
import com.swire.customer_service.response.CustomerResponseByCode;
import com.swire.customer_service.response.CustomerResponseByName;
import com.swire.customer_service.services.CustomerService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/swire-datalake/api/v1/customers")
public class CustomerServiceController {
	
	private static final Logger LOG = LoggerFactory.getLogger(CustomerServiceController.class);
	
	@Autowired
	CustomerService customerService;

	@GetMapping(path = "/")
	@ApiOperation(value="Get Customers.", notes="This api will get the customers list.")
	public CustomerResponse getCustomerData(@ApiParam(value="The offset argument is used to identify the starting point to return rows from a result set. Basically, it excludes the first set of records.", defaultValue="0") @RequestParam("offset") String offset,
			@ApiParam(value="The limit argument is used to limit the number of rows returned in a response result.", defaultValue="100") @RequestParam("limit") String limit,
			@ApiParam(value="Allow ascending and descending sorting over multiple fields. Use (-) for Descending, (+) for Ascending. e.g -COLUMN_NAME or +COLUMN_NAME") @RequestParam("sort") String sort,
			@ApiParam(value="The fields argument is used to fetch specific attributes if it is empty then it will return all attributes.")  @RequestParam("fields") String fields, 
			@ApiParam(value="The fields argument is used to fetch specific attributes if it is empty then it will return all attributes.") @RequestParam("query") String query,
			@ApiParam(value="The date_from argument is used to filter the data. it will return the onwards data with date_from value.", defaultValue="2011-07-19" ) @RequestParam("date_from") String date_from, 
			@ApiParam(value="The date_to argument is used to filter the data. it will return the backward data with date_to value.", defaultValue="2019-12-31") @RequestParam("date_to") String date_to) throws CutomerViewException {
		
		LOG.info("Inside getCustomerData method");
		try {
			return customerService.getCustomers(offset, limit, sort, fields, query, date_from, date_to);
		}catch(Exception e) {
			
			LOG.error(e.getMessage(),  e);
			throw new CutomerViewException("error");
		}
		

	}
	
	@GetMapping(path="/code/{customerCode}")
	@ApiOperation(value="Get Customer By customerCode.", notes="This api will get customer by customerCode.")
	public CustomerResponseByCode getByCode(@ApiParam(value="Customer Code of the customer to get") @PathVariable String customerCode) {
		LOG.info("Inside getByCode method");

		return customerService.getByCode(customerCode);
	}
	
	@GetMapping(path="/name/{customerName}")
	@ApiOperation(value="Get Customer By customerName.", notes="This api will get customer by customerName.")
	public CustomerResponseByName getByName(@ApiParam(value="Customer Name of the customer to get") @PathVariable String customerName){
		LOG.info("Insid getByName method");
		
		return customerService.getByName(customerName);
	}
	
}

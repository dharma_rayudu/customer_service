package com.swire.customer_service.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * For sending the customer request object.
 *
 */
@Getter
@Setter
@ToString
public class CustomerRequest {

	private String offset;
	private String limit;
	private String query;
	private String date_from;
	private String date_to;
}

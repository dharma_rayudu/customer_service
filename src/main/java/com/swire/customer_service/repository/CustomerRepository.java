package com.swire.customer_service.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.swire.customer_service.entity.Customer;


@Repository
public interface CustomerRepository extends CrudRepository<Customer, String>{
	Customer findByCode(String customerCode);
	Customer findByName(String customerName);
}

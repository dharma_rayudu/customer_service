package com.swire.customer_service.validation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.swire.customer_service.exception.CutomerViewException;

@Component
public class CustomerRequestValidation {
	
	public boolean validateRequest(String date_from, String date_to, String fields,
			String limit, String offset, String query, String sort) throws ParseException, CutomerViewException{
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-DD");
		Date d = dateFormat.parse("2011-07-19");
		
		if( date_from.equals(d) ) {
			throw new CutomerViewException("Provide Proper formate.");
		}else {
			return true;
		}
			
	}

}

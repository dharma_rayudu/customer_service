package com.swire.customer_service.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "TABLE_CUSTOMER")
@Data
public class Customer {
	@Id
	private String customer;

	@Column(name = "customer_name")
	private String customerName;

	@Column(name = "last_update_user")
	private String lastUpdateUser;

	@Column(name = "last_update_date")
	private Date lastUpdateDate;
	
	@Column(name = "LAST_UPDATE_TIME")
	private int lastUpdateTime;

	@Column(name = "customer_address_1")
	private String customerAddress1;

	@Column(name = "customer_address_2")
	private String customerAddress2;

	@Column(name = "customer_address_3")
	private String customerAddress3;

	@Column(name = "customer_address_4")
	private String customerAddress4;

	@Column(name = "currency")
	private String currency;

	@Column(name = "lcc")
	private String lcc;

	@Column(name = "phone_no")
	private String phoneNo;
	
	@Column(name = "fax_no")
	private String faxNo;

	@Column(name = "telex_no")
	private String telexNo;

	@Column(name = "customer_contact_1")
	private String customerContact1;

	@Column(name = "customer_contact_2")
	private String customerContact2;

	@Column(name = "CONTACT_1_PHONE")
	private String contact_1_phone;

	@Column(name = "CONTACT_2_PHONE")
	private String contact_2_phone;

	@Column(name = "CONSOLIDATED")
	private String consolidated;

	@Column(name = "SALES_REP")
	private String salesRep;

	@Column(name = "accounting_code")
	private String accountingCode;

	@Column(name = "sailing_schedule")
	private String sailingSchedule;

	@Column(name = "reminder_days")
	private int reminder_days;

	@Column(name = "due_days")
	private int dueDays;

	@Column(name = "prearrival_notice")
	private String prearrivalNotice;

	@Column(name = "arrival_notice")
	private String arrivalNotice;

	@Column(name = "deviation_reporting")
	private String deviationReporting;

	@Column(name = "commission_identifier")
	private String commissionIdentifier;

	@Column(name = "customer_lessee")
	private String customerLessee;

	@Column(name = "customer_lessor")
	private String customer_lessor;

	@Column(name = "customer_haulier")
	private String customerHaulier;

	@Column(name = "customer_bl_party")
	private String customerBlParty;

	@Column(name = "freight_control_identifier")
	private String freightControlIdentifier;

	@Column(name = "PORT_CODE")
	private String portCode;

	@Column(name = "BANK_ACCOUNT")
	private String bankAccount;

	@Column(name = "CREDIT_LIMIT")
	private int creditLimit;

	@Column(name = "POSTCODE")
	private String postcode;

	@Column(name = "vat_no")
	private String vatNo;

	@Column(name = "language")
	private String language;

	@Column(name = "outbound_text")
	private String outboundText;

	@Column(name = "commercial_text")
	private String commercialText;

	@Column(name = "inbound_text")
	private String inboundText;

	@Column(name = "personal_text")
	private String personalText;

	@Column(name = "transmit_indicator")
	private String transmitIndicator;

	@Column(name = "partial_dtcr_ind")
	private String partialDtcrInd;

	@Column(name = "customer_class")
	private String customerClass;

	@Column(name = "billing_office")
	private String billingOffice;

	@Column(name = "dunning_office")
	private String dunningOffice;

	@Column(name = "customer_entering")
	private String customerEntering;
	
	@Column(name = "bank")
	private String bank;

	@Column(name = "credit_agreement")
	private String creditAgreement;

	@Column(name = "credit_limit_currency")
	private String creditLimitCurrency;

	@Column(name = "monthly_overdue_interest")
	private int monthlyOverdueInterest;

	@Column(name = "credit_limit_expiry")
	private Date creditLimitExpiry;

	@Column(name = "expiry_date_user")
	private String expiryDateUser;
	@Column(name = "booking_stop")
	private String bookingStop;

	@Column(name = "booking_stop_user")
	private String bookingStopUser;

	@Column(name = "deviate_credit_limit")
	private String deviateCreditLimit;

	@Column(name = "deviate_credit_user")
	private String deviateCreditUser;

	@Column(name = "state_code")
	private String stateCode;

	@Column(name = "freighted_an")
	private String freighted_an;

	@Column(name = "city_code")
	private String cityCode;

	@Column(name = "country")
	private String country;

	@Column(name = "contact_1_email")
	private String contact_1_email;

	@Column(name = "contact_2_email")
	private String contact_2_email;

	@Column(name = "un_location_code")
	private String unLocationCode;

	@Column(name = "cons_inv_flag")
	private String consInvFlag;

	@Column(name = "customer_id")
	private int customerId;

	@Column(name = "system_status")
	private String systemStatus;

	@Column(name = "approval_level")
	private String approvalLevel;

	@Column(name = "approval_user")
	private String approvalUser;

	@Column(name = "approval_date")
	private Date approvalDate;

	@Column(name = "apply_from")
	private Date applyFrom;

	@Column(name = "apply_to")
	private Date applyTo;

	@Column(name = "req_expiry")
	private String reqExpiry;

	@Column(name = "req_rejection")
	private String reqRejection;

	@Column(name = "req_on_hold")
	private String reqOnHold;

	@Column(name = "req_expiry_user")
	private String reqExpiryUser;

	@Column(name = "req_rejection_user")
	private String reqRejectionUser;

	@Column(name = "req_on_hold_user")
	private String reqOnHoldUser;
	@Column(name = "req_reason")
	private String reqReason;

	@Column(name = "alternate_customer")
	private String alternateCustomer;

	@Column(name = "remarks")
	private String remarks;

	@Column(name = "tariff_sequence_no")
	private String tariffSequenceNo;
	
	@Column(name = "ebill_enabled")
	private String ebillEnabled;
	
	@Column(name = "do_not_use")
	private String doNotUse;

}
